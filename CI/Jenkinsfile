#!/usr/bin/env groovy

pipeline {
  agent none
  options {
    disableConcurrentBuilds()
  }
  stages {
    stage('Init, build parse and increment') {
      agent {
        docker { image 'maven:3.8.7-eclipse-temurin-8-alpine' // create temporaty docker agent with maven
        args '-v $HOME/.m2:/root/.m2' // Directory for maven project dependencies
        reuseNode true
        }
      }
      steps {
        script {
          echo 'incrementing app version...'
          def matcher = readFile('pom.xml') =~ '<version>(.+)</version>' // parse pom.xml
          def version = matcher[0][1]
          env.IMAGE_NAME_BUILD = "archeapxa/demo-app:$version-$BUILD_NUMBER" // image name with build number
        }
      }
    }
    stage("test") {
      agent {
        docker { image 'maven:3.8.7-eclipse-temurin-8-alpine' 
        args '-v $HOME/.m2:/root/.m2'
        reuseNode true
        }
      }
      steps {
        script {
          sh "mvn test" // run maven test
        }
      }
    }
    stage('build app') {
      // when {
      //   expression { env.BRANCH_NAME != 'main' }
      // }
      agent {
        docker { image 'maven:3.8.7-eclipse-temurin-8-alpine' 
        args '-v $HOME/.m2:/root/.m2'
        reuseNode true
        }
      }
      steps {
        script {
          echo "building the application..."
          sh 'mvn clean package' // run maven package to jar
        }
      }
    }
    stage('build image') {
      // when {
      //   expression { env.BRANCH_NAME != 'main' }
      // }
      agent {
        docker { image 'docker:latest' 
        args '--rm -ti --group-add 997 -v /var/run/docker.sock:/var/run/docker.sock' // docker in docker fix
        reuseNode true
        }
      }
      environment { 
        DOCKER_CONFIG = "${WORKSPACE}/docker.config" // temp docker config file
      }
      steps {
        script {
          echo "building the docker image..."
          withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            sh "docker build -t ${IMAGE_NAME_BUILD} ." // build image from dockerfile
            sh "echo $PASS | docker login -u $USER --password-stdin" // pass passed to dockerhub login via STDIN channel
            sh "docker push ${IMAGE_NAME_BUILD}" // push build to dockerhub
            sh "docker image rm ${IMAGE_NAME_BUILD}" // remove build from agent
          }
        }
      }
    }
    stage('trigger CD') {
      steps {
        script {
          echo "Triggering CD job for branch $BRANCH_NAME" // trigger CD and pass build name
          build  wait: false, job: "../demo-app-CD/$BRANCH_NAME", parameters: [string(name: 'IMAGE_NAME', value: "${IMAGE_NAME_BUILD}")]
        }
      }
    }
  }
}
