provider "google" {
  project     = var.project_id
  region      = var.region
  zone        = var.zone
  # credentials = var.gcp_cred
}

# resource "random_id" "bucket_prefix" {
#   byte_length = 8
# }

resource "google_storage_bucket" "tfstate_bucket" {
  # name          = "${random_id.bucket_prefix.hex}-${var.env}-bucket-tfstate"
  name          = "java-maven-demo-${var.env}-bucket-tfstate"
  force_destroy = false
  location      = "US"
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }
}

module "app-network" {
  source = "./modules/network"
  env = var.env
  region = var.region
  subnet_cidr_range = var.subnet_cidr_range
  create_subnets = false
}


# Create a single Compute Engine instance
resource "google_compute_instance" "app_vm" {
  name         = "${var.env}-env"
  machine_type = var.machine_type
  zone         = var.zone
  tags         = ["ssh"]
  labels       = {
    env = "${var.env}"
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    subnetwork = module.app-network.subnet.id
    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}






# resource "google_compute_network" "app_vpc_network" {
#   name                    = "${var.env}-app-vpc"
#   auto_create_subnetworks = false
#   mtu                     = 1460
# }

# resource "google_compute_subnetwork" "app_subnetwork" {
#   name          = "${var.env}-app-subnet"
#   ip_cidr_range = var.ip_cidr_range
#   region        = var.region
#   network       = google_compute_network.app_vpc_network.id
# }

# resource "google_compute_firewall" "app_firewall" {
#   name    = "${var.env}-firewall-allow-rules"
#   network = google_compute_network.app_vpc_network.name

#   allow {
#     protocol = "icmp"
#   }

#   allow {
#     protocol = "tcp"
#     ports    = ["22"]
#   }

#   allow {
#     protocol = "tcp"
#     ports    = ["80", "8080"]
#   }

#   source_ranges = ["0.0.0.0/0"]

# }