variable "env" {
  description = "environment"
}

variable "project_id" {
  description = "id of a project"
}

variable "region" {
  description = "region to use"
}
variable "zone" {
  description = "zone to use"
}
variable "subnet_cidr_range" {
  description = "cidr for subnet"
}
variable "machine_type" {
  description = "vm type"
}