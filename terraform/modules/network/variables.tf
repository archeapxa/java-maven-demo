variable "create_subnets" {}

variable "env" {
  description = "environment"
}

variable "region" {
  description = "region to use"
}

variable "subnet_cidr_range" {
  description = "cidr for subnet"
}