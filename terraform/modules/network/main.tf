

resource "google_compute_network" "app_vpc_network" {
  name                    = "${var.env}-app-vpc"
  auto_create_subnetworks = var.create_subnets
  mtu                     = 1460
}

resource "google_compute_subnetwork" "app_subnetwork" {
  name          = "${var.env}-app-subnet"
  ip_cidr_range = var.subnet_cidr_range
  region        = var.region
  network       = google_compute_network.app_vpc_network.id
}

resource "google_compute_firewall" "app_firewall_icmp" {
  name    = "${var.env}-firewall-allow-icmp"
  network = google_compute_network.app_vpc_network.name

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]

}

resource "google_compute_firewall" "app_firewall_ssh" {
  name    = "${var.env}-firewall-allow-ssh"
  network = google_compute_network.app_vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]

}

resource "google_compute_firewall" "app_firewall_tcp" {
  name    = "${var.env}-firewall-allow-tcp"
  network = google_compute_network.app_vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["80", "8080"]
  }

  source_ranges = ["0.0.0.0/0"]

}