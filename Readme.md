# EPAM Cloud and DevOps Fundamentals Autumn 2022

## Stanislav Iushkevich`s Final Project

### CI/CD Pipeline for Java-Maven demo application

- App itself contained in directory src and pox.xml file
- App includes simple tests
- Application displays a simple web page on port 8080 when started, nothing more

#### Jenkins

- Two different Jenkinsfile contained in "CI" and "CD" folders
- Each stage runs on a different agent, which starts as docker container on a Jenkins host machine just for one stage
- CI pipeline has a parametrized trigger for CD pipeline in a last stage
- CD pipeline uses Terraform and Ansible to create infrastructure and deploy to it
- Git branch name are used as environment variable, which passed to Ansible and Terraform as an environment name
- Jenkins waits for input to know whether to destroy infrastructure at the end of CD

#### Terraform

- Terraform uses GCP as a provider
- tfstate for each environment saved in different bucket on GCP
- Creating of network, subnetwork and firewall rules are done as a tf module

#### Ansible

- Waits for ssh port to open on a targeted VM
- GCP inventory plugin used to get a list of hosts
- Playbook divided to roles
- Install dependencies in a first role
- Deploy app in a second role
